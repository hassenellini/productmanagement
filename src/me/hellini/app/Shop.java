package me.hellini.app;

import me.hellini.data.Product;

import java.math.BigDecimal;

public class Shop {

    public static void main(String[] args) {
        // write your code here
        Product p1 = new Product();
        p1.setId(101);
        p1.setName("Tea");
        p1.setPrice(BigDecimal.valueOf(1.99));
        System.out.println(p1);
    }
}
